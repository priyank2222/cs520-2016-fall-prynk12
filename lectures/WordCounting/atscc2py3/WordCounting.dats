(*
** Enumerate the words
** according to their frequencies
** in a given file.
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2PY3}/staloadall.hats"
//
(* ****** ****** *)

#define WordCounting 1

(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
local
#include "./WordCounting_lib.dats"
in (*nothing*) end
//
(* ****** ****** *)

(* end of [WordCounting.dats] *)
