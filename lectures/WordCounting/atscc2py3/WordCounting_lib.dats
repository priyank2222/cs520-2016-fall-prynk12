(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
#ifdef
WordCounting
#then
#else
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2PY3}/staloadall.hats"
#endif // ifdef(WordCounting)
//
(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
#include
"./../share/WordCounting_lib.dats"
//
(* ****** ****** *)

(* end of [WordCounting_lib.dats] *)
