(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
#ifdef
WordCounting
#then
#else
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
#endif // ifdef(WordCounting)
//
(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
#include
"./../share/WordCounting_lib.dats"
//
(* ****** ****** *)

implement
word_get() = let
//
fun
skip(): int = let
  val c = char_get()
(*
  val () = println! ("skip: c = ", c)
*)
in
  if isalpha(c) then c
    else (if c >= 0 then skip() else ~1)
  // end of [if]
end
//
fun
get2
(
  cs: list0(char)
) : list0(char) = let
  val c = char_get()
in
//
if isalpha(c) then
  get2(list0_cons(tolower(int2char0(c)), cs)) else cs
// end of [if]
//
end // end of [get2]
//
val c0 = skip()
//
in
//
if
c0 >= 0
then
string_make_list
(
  list0_reverse<char>(get2(list0_sing(tolower(int2char0(c0)))))
) (* end of [then] *)
else "" // end of [else]
//
end // end of [word_get]

(* ****** ****** *)
//
implement
mylist_sort_string
  (xs) =
list0_mergesort<string>
  (xs, lam(x1, x2) => compare(x1, x2))
implement
mylist_sort_intstr
  (nxs) =
list0_mergesort<intstr>
  ( nxs
  , lam(nx1, nx2) =>
    let val sgn = ~compare(nx1.0, nx2.0)
    in
      if sgn != 0 then sgn else compare(nx1.1, nx2.1)
    end
  )
//
(* ****** ****** *)

(* end of [WordCounting_lib.dats] *)
