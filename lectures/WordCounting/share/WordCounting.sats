(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
#ifdef
ATSOPT
#then
staload "libats/ML/SATS/basis.sats"
#endif // ifdef(ATSOPT)
//
(* ****** ****** *)
//
#ifdef
ATSCC2PY3
#then
#include "share/atspre_define.hats"
#staload "{$LIBATSCC2PY3}/basics_py.sats"
#endif // ifdef(ATSCC2PY3)
//
(* ****** ****** *)
//
fun
char_get(): int
//
fun
word_get(): string
//
(* ****** ****** *)
//
fun
theWords_get_all(): list0(string)
//
(* ****** ****** *)
//
typedef
intstr = $tup(int, string)
//
(* ****** ****** *)
//
fun
mylist_sort_string
  : list0(string) -> list0(string)
fun
mylist_sort_intstr
  : list0(intstr) -> list0(intstr)
//
(* ****** ****** *)
//
fun
mylist_group
  (xs: list0(string)): list0(intstr)
//
(* ****** ****** *)
//
fun
WordCounting_main(): list0(intstr)
//
(* ****** ****** *)

(* end of [WordCounting.sats] *)
