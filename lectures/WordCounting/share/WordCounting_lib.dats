(*
** Enumerate words
** according to their frequencies
** in a given source
*)

(* ****** ****** *)
//
implement
theWords_get_all() = let
//
fun
aux
(
  ws: list0(string)
) : list0(string) =
(
let val w = word_get()
  in if w != "" then aux(list0_cons(w, ws)) else ws end
)
//
in
  aux(list0_nil)
end // end of [theWords_get]

(* ****** ****** *)
//
implement
mylist_group
  (xs) = let
//
fun
loop
(
  n0: int
, x0: string
, xs: list0(string)
, res: list0(intstr)
) : list0 (intstr) =
(
case xs of
| list0_nil() =>
    list0_cons{intstr}($tup(n0, x0), res)
  // list0_nil
| list0_cons(x1, xs) =>
  (
    if x0 = x1
      then loop(n0+1, x0, xs, res)
      else let
        val res = list0_cons{intstr}($tup(n0, x0), res)
      in
        loop(1, x1, xs, res)
      end
    // end of [if]
  )
) (* end of [loop] *)
//
in
  case xs of
  | list0_nil() => list0_nil()
  | list0_cons(x, xs) => loop(1, x, xs, list0_nil)
end // end of [mylist_group]
//
(* ****** ****** *)
//
implement
WordCounting_main() =
  nws where
{
  val ws =
    theWords_get_all()
  // end of [val]
(*
  val () =
  println!
    ("length(ws) = ", list0_length(ws))
  // end of [val]
*)
  val ws =
    mylist_sort_string(ws)
  // end of [val]
//
  val nws = mylist_group (ws)
(*
  val () =
  println!
    ("length(nws) = ", list0_length(nws))
  // end of [val]
*)
//
  val nws = mylist_sort_intstr (nws) 
//
} (* end of [WordCounting_main] *)
//
(* ****** ****** *)

(* end of [WordCounting_lib.dats] *)
