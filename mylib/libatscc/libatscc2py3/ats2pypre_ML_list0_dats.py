######
##
## The Python3 code
## is generated from ATS source by atscc2py3
## The starting compilation time is: 2016-9-8: 11h:47m
##
######

######
#ATSextcode_beg()
######
######
from ats2pypre_basics_cats import *
######
from ats2pypre_integer_cats import *
from ats2pypre_bool_cats import *
######
from ats2pypre_list_dats import *
######
######
#ATSextcode_end()
######

def _ats2pypre_ML_list0_patsfun_22__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_22__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_22(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_22__cfun, env0)

def _ats2pypre_ML_list0_patsfun_25__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_25__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_25(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_25__cfun, env0)

def _ats2pypre_ML_list0_patsfun_28__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_28__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_28(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_28__cfun, env0)

def _ats2pypre_ML_list0_patsfun_31__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_31__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_31(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_31__cfun, env0)

def _ats2pypre_ML_list0_patsfun_35__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_35__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_35(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_35__cfun, env0)

def _ats2pypre_ML_list0_patsfun_38__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_38__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_38(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_38__cfun, env0)

def _ats2pypre_ML_list0_patsfun_41__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_41__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_41(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_41__cfun, env0)

def _ats2pypre_ML_list0_patsfun_44__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_44__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_44(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_44__cfun, env0)

def _ats2pypre_ML_list0_patsfun_47__closurerize(env0):
  def _ats2pypre_ML_list0_patsfun_47__cfun(cenv, arg0): return _ats2pypre_ML_list0_patsfun_47(cenv[1], arg0)
  return (_ats2pypre_ML_list0_patsfun_47__cfun, env0)

def ats2pypre_ML_list0_head_opt(arg0):
  tmpret2 = None
  tmp3 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab6():
    nonlocal arg0
    nonlocal tmpret2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab9
    __atstmplab7()
    return
  def __atstmplab7():
    nonlocal arg0
    nonlocal tmpret2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret2 = None
    return
  def __atstmplab8():
    nonlocal arg0
    nonlocal tmpret2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab9()
    return
  def __atstmplab9():
    nonlocal arg0
    nonlocal tmpret2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp3 = arg0[0]
    tmpret2 = (tmp3, )
    return
  mbranch_1 = { 1: __atstmplab6, 2: __atstmplab7, 3: __atstmplab8, 4: __atstmplab9 }
  #__patsflab_list0_head_opt
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret2


def ats2pypre_ML_list0_tail_opt(arg0):
  tmpret5 = None
  tmp7 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab10():
    nonlocal arg0
    nonlocal tmpret5, tmp7
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab13
    __atstmplab11()
    return
  def __atstmplab11():
    nonlocal arg0
    nonlocal tmpret5, tmp7
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret5 = None
    return
  def __atstmplab12():
    nonlocal arg0
    nonlocal tmpret5, tmp7
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab13()
    return
  def __atstmplab13():
    nonlocal arg0
    nonlocal tmpret5, tmp7
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp7 = arg0[1]
    tmpret5 = (tmp7, )
    return
  mbranch_1 = { 1: __atstmplab10, 2: __atstmplab11, 3: __atstmplab12, 4: __atstmplab13 }
  #__patsflab_list0_tail_opt
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret5


def ats2pypre_ML_list0_length(arg0):
  tmpret8 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_length
  tmpret8 = ats2pypre_list_length(arg0)
  return tmpret8


def ats2pypre_ML_list0_last_opt(arg0):
  tmpret9 = None
  tmp13 = None
  tmp14 = None
  tmp15 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab18():
    nonlocal arg0
    nonlocal tmpret9, tmp13, tmp14, tmp15
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab21
    __atstmplab19()
    return
  def __atstmplab19():
    nonlocal arg0
    nonlocal tmpret9, tmp13, tmp14, tmp15
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret9 = None
    return
  def __atstmplab20():
    nonlocal arg0
    nonlocal tmpret9, tmp13, tmp14, tmp15
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab21()
    return
  def __atstmplab21():
    nonlocal arg0
    nonlocal tmpret9, tmp13, tmp14, tmp15
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp13 = arg0[0]
    tmp14 = arg0[1]
    tmp15 = _ats2pypre_ML_list0_loop_6(tmp13, tmp14)
    tmpret9 = (tmp15, )
    return
  mbranch_1 = { 1: __atstmplab18, 2: __atstmplab19, 3: __atstmplab20, 4: __atstmplab21 }
  #__patsflab_list0_last_opt
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret9


def _ats2pypre_ML_list0_loop_6(arg0, arg1):
  apy0 = None
  apy1 = None
  tmpret10 = None
  tmp11 = None
  tmp12 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab14():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret10, tmp11, tmp12
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg1)): tmplab_py = 4 ; return#__atstmplab17
    __atstmplab15()
    return
  def __atstmplab15():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret10, tmp11, tmp12
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret10 = arg0
    return
  def __atstmplab16():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret10, tmp11, tmp12
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab17()
    return
  def __atstmplab17():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret10, tmp11, tmp12
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp11 = arg1[0]
    tmp12 = arg1[1]
    #ATStailcalseq_beg
    apy0 = tmp11
    apy1 = tmp12
    arg0 = apy0
    arg1 = apy1
    funlab_py = 1 #__patsflab__ats2pypre_ML_list0_loop_6
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab14, 2: __atstmplab15, 3: __atstmplab16, 4: __atstmplab17 }
  while(1):
    funlab_py = 0
    #__patsflab__ats2pypre_ML_list0_loop_6
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret10


def ats2pypre_ML_list0_get_at_opt(arg0, arg1):
  apy0 = None
  apy1 = None
  tmpret16 = None
  tmp17 = None
  tmp18 = None
  tmp19 = None
  tmp20 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab22():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret16, tmp17, tmp18, tmp19, tmp20
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab25
    __atstmplab23()
    return
  def __atstmplab23():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret16, tmp17, tmp18, tmp19, tmp20
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret16 = None
    return
  def __atstmplab24():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret16, tmp17, tmp18, tmp19, tmp20
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab25()
    return
  def __atstmplab25():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret16, tmp17, tmp18, tmp19, tmp20
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp17 = arg0[0]
    tmp18 = arg0[1]
    tmp19 = ats2pypre_gt_int1_int1(arg1, 0)
    if (tmp19):
      tmp20 = ats2pypre_sub_int1_int1(arg1, 1)
      #ATStailcalseq_beg
      apy0 = tmp18
      apy1 = tmp20
      arg0 = apy0
      arg1 = apy1
      funlab_py = 1 #__patsflab_list0_get_at_opt
      #ATStailcalseq_end
    else:
      tmpret16 = (tmp17, )
    #endif
    return
  mbranch_1 = { 1: __atstmplab22, 2: __atstmplab23, 3: __atstmplab24, 4: __atstmplab25 }
  while(1):
    funlab_py = 0
    #__patsflab_list0_get_at_opt
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret16


def ats2pypre_ML_list0_make_intrange_2(arg0, arg1):
  tmpret21 = None
  tmp22 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_make_intrange_2
  tmp22 = ats2pypre_list_make_intrange_2(arg0, arg1)
  tmpret21 = tmp22
  return tmpret21


def ats2pypre_ML_list0_make_intrange_3(arg0, arg1, arg2):
  tmpret23 = None
  tmp24 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_make_intrange_3
  tmp24 = ats2pypre_list_make_intrange_3(arg0, arg1, arg2)
  tmpret23 = tmp24
  return tmpret23


def ats2pypre_ML_list0_snoc(arg0, arg1):
  tmpret36 = None
  tmp37 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_snoc
  tmp37 = ats2pypre_list_snoc(arg0, arg1)
  tmpret36 = tmp37
  return tmpret36


def ats2pypre_ML_list0_extend(arg0, arg1):
  tmpret38 = None
  tmp39 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_extend
  tmp39 = ats2pypre_list_extend(arg0, arg1)
  tmpret38 = tmp39
  return tmpret38


def ats2pypre_ML_list0_append(arg0, arg1):
  tmpret40 = None
  tmp41 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_append
  tmp41 = ats2pypre_list_append(arg0, arg1)
  tmpret40 = tmp41
  return tmpret40


def ats2pypre_ML_list0_reverse(arg0):
  tmpret42 = None
  tmp43 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_reverse
  tmp43 = ats2pypre_list_reverse(arg0)
  tmpret42 = tmp43
  return tmpret42


def ats2pypre_ML_list0_reverse_append(arg0, arg1):
  tmpret44 = None
  tmp45 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_reverse_append
  tmp45 = ats2pypre_list_reverse_append(arg0, arg1)
  tmpret44 = tmp45
  return tmpret44


def ats2pypre_ML_list0_exists(arg0, arg1):
  tmpret46 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_exists
  tmpret46 = ats2pypre_list_exists(arg0, arg1)
  return tmpret46


def ats2pypre_ML_list0_exists_method(arg0):
  tmpret47 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_exists_method
  tmpret47 = _ats2pypre_ML_list0_patsfun_22__closurerize(arg0)
  return tmpret47


def _ats2pypre_ML_list0_patsfun_22(env0, arg0):
  tmpret48 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_22
  tmpret48 = ats2pypre_ML_list0_exists(env0, arg0)
  return tmpret48


def ats2pypre_ML_list0_iexists(arg0, arg1):
  tmpret49 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iexists
  tmpret49 = ats2pypre_list_iexists(arg0, arg1)
  return tmpret49


def ats2pypre_ML_list0_iexists_method(arg0):
  tmpret50 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iexists_method
  tmpret50 = _ats2pypre_ML_list0_patsfun_25__closurerize(arg0)
  return tmpret50


def _ats2pypre_ML_list0_patsfun_25(env0, arg0):
  tmpret51 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_25
  tmpret51 = ats2pypre_ML_list0_iexists(env0, arg0)
  return tmpret51


def ats2pypre_ML_list0_forall(arg0, arg1):
  tmpret52 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_forall
  tmpret52 = ats2pypre_list_forall(arg0, arg1)
  return tmpret52


def ats2pypre_ML_list0_forall_method(arg0):
  tmpret53 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_forall_method
  tmpret53 = _ats2pypre_ML_list0_patsfun_28__closurerize(arg0)
  return tmpret53


def _ats2pypre_ML_list0_patsfun_28(env0, arg0):
  tmpret54 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_28
  tmpret54 = ats2pypre_ML_list0_forall(env0, arg0)
  return tmpret54


def ats2pypre_ML_list0_iforall(arg0, arg1):
  tmpret55 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iforall
  tmpret55 = ats2pypre_list_iforall(arg0, arg1)
  return tmpret55


def ats2pypre_ML_list0_iforall_method(arg0):
  tmpret56 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iforall_method
  tmpret56 = _ats2pypre_ML_list0_patsfun_31__closurerize(arg0)
  return tmpret56


def _ats2pypre_ML_list0_patsfun_31(env0, arg0):
  tmpret57 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_31
  tmpret57 = ats2pypre_ML_list0_iforall(env0, arg0)
  return tmpret57


def ats2pypre_ML_list0_app(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_app
  ats2pypre_ML_list0_foreach(arg0, arg1)
  return#_void


def ats2pypre_ML_list0_foreach(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_foreach
  ats2pypre_list_foreach(arg0, arg1)
  return#_void


def ats2pypre_ML_list0_foreach_method(arg0):
  tmpret60 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_foreach_method
  tmpret60 = _ats2pypre_ML_list0_patsfun_35__closurerize(arg0)
  return tmpret60


def _ats2pypre_ML_list0_patsfun_35(env0, arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_35
  ats2pypre_ML_list0_foreach(env0, arg0)
  return#_void


def ats2pypre_ML_list0_iforeach(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iforeach
  ats2pypre_list_iforeach(arg0, arg1)
  return#_void


def ats2pypre_ML_list0_iforeach_method(arg0):
  tmpret63 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_iforeach_method
  tmpret63 = _ats2pypre_ML_list0_patsfun_38__closurerize(arg0)
  return tmpret63


def _ats2pypre_ML_list0_patsfun_38(env0, arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_38
  ats2pypre_ML_list0_iforeach(env0, arg0)
  return#_void


def ats2pypre_ML_list0_rforeach(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_rforeach
  ats2pypre_list_rforeach(arg0, arg1)
  return#_void


def ats2pypre_ML_list0_rforeach_method(arg0):
  tmpret66 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_rforeach_method
  tmpret66 = _ats2pypre_ML_list0_patsfun_41__closurerize(arg0)
  return tmpret66


def _ats2pypre_ML_list0_patsfun_41(env0, arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_41
  ats2pypre_ML_list0_rforeach(env0, arg0)
  return#_void


def ats2pypre_ML_list0_filter(arg0, arg1):
  tmpret68 = None
  tmp69 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_filter
  tmp69 = ats2pypre_list_filter(arg0, arg1)
  tmpret68 = tmp69
  return tmpret68


def ats2pypre_ML_list0_filter_method(arg0):
  tmpret70 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_filter_method
  tmpret70 = _ats2pypre_ML_list0_patsfun_44__closurerize(arg0)
  return tmpret70


def _ats2pypre_ML_list0_patsfun_44(env0, arg0):
  tmpret71 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_44
  tmpret71 = ats2pypre_ML_list0_filter(env0, arg0)
  return tmpret71


def ats2pypre_ML_list0_map(arg0, arg1):
  tmpret72 = None
  tmp73 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_map
  tmp73 = ats2pypre_list_map(arg0, arg1)
  tmpret72 = tmp73
  return tmpret72


def ats2pypre_ML_list0_map_method(arg0, arg1):
  tmpret74 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_map_method
  tmpret74 = _ats2pypre_ML_list0_patsfun_47__closurerize(arg0)
  return tmpret74


def _ats2pypre_ML_list0_patsfun_47(env0, arg0):
  tmpret75 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab__ats2pypre_ML_list0_patsfun_47
  tmpret75 = ats2pypre_ML_list0_map(env0, arg0)
  return tmpret75


def ats2pypre_ML_list0_foldleft(arg0, arg1, arg2):
  tmpret76 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_foldleft
  tmpret76 = _ats2pypre_ML_list0_aux_49(arg2, arg1, arg0)
  return tmpret76


def _ats2pypre_ML_list0_aux_49(env0, arg0, arg1):
  apy0 = None
  apy1 = None
  tmpret77 = None
  tmp78 = None
  tmp79 = None
  tmp80 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab30():
    nonlocal env0, arg0, arg1
    nonlocal apy0, apy1, tmpret77, tmp78, tmp79, tmp80
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg1)): tmplab_py = 4 ; return#__atstmplab33
    __atstmplab31()
    return
  def __atstmplab31():
    nonlocal env0, arg0, arg1
    nonlocal apy0, apy1, tmpret77, tmp78, tmp79, tmp80
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret77 = arg0
    return
  def __atstmplab32():
    nonlocal env0, arg0, arg1
    nonlocal apy0, apy1, tmpret77, tmp78, tmp79, tmp80
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab33()
    return
  def __atstmplab33():
    nonlocal env0, arg0, arg1
    nonlocal apy0, apy1, tmpret77, tmp78, tmp79, tmp80
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp78 = arg1[0]
    tmp79 = arg1[1]
    tmp80 = env0[0](env0, arg0, tmp78)
    #ATStailcalseq_beg
    apy0 = tmp80
    apy1 = tmp79
    arg0 = apy0
    arg1 = apy1
    funlab_py = 1 #__patsflab__ats2pypre_ML_list0_aux_49
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab30, 2: __atstmplab31, 3: __atstmplab32, 4: __atstmplab33 }
  while(1):
    funlab_py = 0
    #__patsflab__ats2pypre_ML_list0_aux_49
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret77


def ats2pypre_ML_list0_foldright(arg0, arg1, arg2):
  tmpret81 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_list0_foldright
  tmpret81 = _ats2pypre_ML_list0_aux_51(arg1, arg2, arg0, arg2)
  return tmpret81


def _ats2pypre_ML_list0_aux_51(env0, env1, arg0, arg1):
  tmpret82 = None
  tmp83 = None
  tmp84 = None
  tmp85 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab34():
    nonlocal env0, env1, arg0, arg1
    nonlocal tmpret82, tmp83, tmp84, tmp85
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab37
    __atstmplab35()
    return
  def __atstmplab35():
    nonlocal env0, env1, arg0, arg1
    nonlocal tmpret82, tmp83, tmp84, tmp85
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret82 = arg1
    return
  def __atstmplab36():
    nonlocal env0, env1, arg0, arg1
    nonlocal tmpret82, tmp83, tmp84, tmp85
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab37()
    return
  def __atstmplab37():
    nonlocal env0, env1, arg0, arg1
    nonlocal tmpret82, tmp83, tmp84, tmp85
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp83 = arg0[0]
    tmp84 = arg0[1]
    tmp85 = _ats2pypre_ML_list0_aux_51(env0, env1, tmp84, env1)
    tmpret82 = env0[0](env0, tmp83, tmp85)
    return
  mbranch_1 = { 1: __atstmplab34, 2: __atstmplab35, 3: __atstmplab36, 4: __atstmplab37 }
  #__patsflab__ats2pypre_ML_list0_aux_51
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret82

######
##
## end-of-compilation-unit
##
######
