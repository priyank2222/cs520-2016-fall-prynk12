######
##
## The Python3 code
## is generated from ATS source by atscc2py3
## The starting compilation time is: 2016-9-8: 11h:47m
##
######

######
#ATSextcode_beg()
######
######
from ats2pypre_basics_cats import *
######
from ats2pypre_integer_cats import *
from ats2pypre_bool_cats import *
######
######
#ATSextcode_end()
######

def _057_home_057_hwxi_057_Research_057_ATS_055_Postiats_055_contrib_057_contrib_057_libatscc_057_libatscc2py3_057_SATS_057_PYlist_056_sats__PYlist_oflist(arg0):
  tmpret0 = None
  tmp5 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_PYlist_oflist
  tmp5 = ats2pypre_PYlist_nil()
  tmpret0 = _ats2pypre_PYlist_aux_1(arg0, tmp5)
  return tmpret0


def _ats2pypre_PYlist_aux_1(arg0, arg1):
  apy0 = None
  apy1 = None
  tmpret1 = None
  tmp2 = None
  tmp3 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab0():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret1, tmp2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab3
    __atstmplab1()
    return
  def __atstmplab1():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret1, tmp2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret1 = arg1
    return
  def __atstmplab2():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret1, tmp2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab3()
    return
  def __atstmplab3():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret1, tmp2, tmp3
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp2 = arg0[0]
    tmp3 = arg0[1]
    ats2pypre_PYlist_append(arg1, tmp2)
    #ATStailcalseq_beg
    apy0 = tmp3
    apy1 = arg1
    arg0 = apy0
    arg1 = apy1
    funlab_py = 1 #__patsflab__ats2pypre_PYlist_aux_1
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab0, 2: __atstmplab1, 3: __atstmplab2, 4: __atstmplab3 }
  while(1):
    funlab_py = 0
    #__patsflab__ats2pypre_PYlist_aux_1
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret1


def _057_home_057_hwxi_057_Research_057_ATS_055_Postiats_055_contrib_057_contrib_057_libatscc_057_libatscc2py3_057_SATS_057_PYlist_056_sats__PYlist_oflist_rev(arg0):
  tmpret6 = None
  tmp11 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_PYlist_oflist_rev
  tmp11 = ats2pypre_PYlist_nil()
  tmpret6 = _ats2pypre_PYlist_aux_3(arg0, tmp11)
  return tmpret6


def _ats2pypre_PYlist_aux_3(arg0, arg1):
  apy0 = None
  apy1 = None
  tmpret7 = None
  tmp8 = None
  tmp9 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab4():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret7, tmp8, tmp9
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab7
    __atstmplab5()
    return
  def __atstmplab5():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret7, tmp8, tmp9
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret7 = arg1
    return
  def __atstmplab6():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret7, tmp8, tmp9
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab7()
    return
  def __atstmplab7():
    nonlocal arg0, arg1
    nonlocal apy0, apy1, tmpret7, tmp8, tmp9
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp8 = arg0[0]
    tmp9 = arg0[1]
    ats2pypre_PYlist_cons(tmp8, arg1)
    #ATStailcalseq_beg
    apy0 = tmp9
    apy1 = arg1
    arg0 = apy0
    arg1 = apy1
    funlab_py = 1 #__patsflab__ats2pypre_PYlist_aux_3
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab4, 2: __atstmplab5, 3: __atstmplab6, 4: __atstmplab7 }
  while(1):
    funlab_py = 0
    #__patsflab__ats2pypre_PYlist_aux_3
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret7

######
##
## end-of-compilation-unit
##
######
