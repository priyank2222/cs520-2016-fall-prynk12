(*
** Proving that
** the 3 side bisectors of a triangle
** are concurrent
*)

(* ****** ****** *)
//
// HX: 10 points
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
staload
"libats/SATS/Number/real.sats"
//
(* ****** ****** *)

stacst x1: real
stacst y1: real // A: (x1, y1)
stacst x2: real
stacst y2: real // B: (x2, y2)
stacst x3: real
stacst y3: real // C: (x3, y3)

(* ****** ****** *)

(* end of [ConcurrentSideBisectors.dats] *)
